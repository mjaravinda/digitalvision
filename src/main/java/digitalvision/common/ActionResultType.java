package digitalvision.common;

public enum ActionResultType {

    OK,
    Error,
    BADRequest,
    Terminated,
    Activated,
    InActive,
    NotExists


}
