package digitalvision.common;

import java.util.List;

public interface IGenaricService<T> {
    List<T> getAll();

    T getById(T t);

    ActionResult save(T t);

    ActionResult delete(T t);
}