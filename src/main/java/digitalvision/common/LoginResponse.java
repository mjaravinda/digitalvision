package digitalvision.common;

import digitalvision.adminuser.AdminUser;
import digitalvision.customer.Customer;

public class LoginResponse {

    private Customer customer;
    private AdminUser adminUser;
    private UserTypeEnum userType;
    private String token;
    private ActionResult result;

    public LoginResponse(Customer customer, AdminUser adminUser, UserTypeEnum userType, String token,
                         ActionResult result) {
        super();
        this.customer = customer;
        this.adminUser = adminUser;
        this.userType = userType;
        this.token = token;
        this.result = result;
    }

    public LoginResponse() {
        super();
    }

    public UserTypeEnum getUserType() {
        return userType;
    }

    public void setUserType(UserTypeEnum userType) {
        this.userType = userType;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public AdminUser getAdminUser() {
        return adminUser;
    }

    public void setAdminUser(AdminUser adminUser) {
        this.adminUser = adminUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ActionResult getResult() {
        return result;
    }

    public void setResult(ActionResult result) {
        this.result = result;
    }

}
