package digitalvision.common;

public class ActionResult {
    public ActionResultType resultType;
    public String message;

    public ActionResult(ActionResultType resultType, String message) {
        super();
        this.resultType = resultType;
        this.message = message;


    }

}
