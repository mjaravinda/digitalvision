package digitalvision.customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import digitalvision.common.StateEnum;
import digitalvision.order.Order;
import digitalvision.wishlist.WishList;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "customers")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Nullable
    private StateEnum State;
    @Nullable
    private java.sql.Timestamp lastAttempedTime;
    @Nullable
    private int attempedCount;
    private String firstName;

    private String lastname;
    private String email;
    private String password;
    private String contactNo;
    @Nullable
    private String billiingAddress;
    @Nullable
    private String activationCode;
    @JsonIgnore
    @OneToMany(mappedBy = "customer")
    private List<WishList> wishLists;
    @JsonIgnore
    @OneToMany(mappedBy = "customer")
    private List<Order> orders;

    public Customer(long id, StateEnum state, Timestamp lastAttempedTime, int attempedCount,
                    String firstName, String lastname, String email, String password, String contactNo, String billiingAddress,
                    String activationCode) {
        super();
        this.id = id;

        State = state;

        this.lastAttempedTime = lastAttempedTime;
        this.attempedCount = attempedCount;
        this.firstName = firstName;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.contactNo = contactNo;
        this.billiingAddress = billiingAddress;
        this.activationCode = activationCode;
    }

    public Customer() {
        super();
    }

    public java.sql.Timestamp getLastAttempedTime() {
        return lastAttempedTime;
    }

    public void setLastAttempedTime(java.sql.Timestamp lastAttempedTime) {
        this.lastAttempedTime = lastAttempedTime;
    }

    public int getAttempedCount() {
        return attempedCount;
    }

    public void setAttempedCount(int attempedCount) {
        this.attempedCount = attempedCount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public List<WishList> getWishLists() {
        return wishLists;
    }

    public void setWishLists(List<WishList> wishLists) {
        this.wishLists = wishLists;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public StateEnum getState() {
        return State;
    }

    public void setState(StateEnum state) {
        State = state;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getBillingAddress() {
        return billiingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billiingAddress = billingAddress;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }
}
