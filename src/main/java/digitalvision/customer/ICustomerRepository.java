package digitalvision.customer;


import digitalvision.adminuser.AdminUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ICustomerRepository extends CrudRepository<Customer, Long> {

    @Query(
            value = "SELECT * FROM customers c WHERE c.state = ?1",
            nativeQuery = true)
    List<Customer> findAll(int state);

    @Query(
            value = "SELECT * FROM customers c WHERE c.email = ?1",
            nativeQuery = true)
    Customer login(String username);


    @Query(
            value = "SELECT * FROM customers c WHERE c.email = ?1 AND c.id !=?2 AND state !=2",
            nativeQuery = true)
    Customer alreadyExistEmailOnCustomer(String username, long id);

    @Query(
            value = "SELECT * FROM admin_users c WHERE c.email = ?1 AND state !=2",
            nativeQuery = true)
    AdminUser alreadyExistEmailOnAdminUser(String username);


}
