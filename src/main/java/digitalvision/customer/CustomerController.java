package digitalvision.customer;

import digitalvision.common.ActionResultType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


//@Api(value="customer", description="used for view,add, edit and delete customer")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "customer", description = "used for view,add, edit and delete customer")
@RequestMapping("/api/")
@RestController
public class CustomerController {
    @Autowired
    private CustomerService service;

    @ApiOperation(value = "View a list of available customers", response = Iterable.class)
    @GetMapping("customer/all")
    public List<Customer> getAll() {

        return service.getAll();
    }

    @GetMapping("customer/getbyid/{id}")
    public Customer getById(@PathVariable("id") long id) {

        Customer cus = new Customer();
        cus.setId(id);
        return service.getById(cus);
    }


    @PostMapping("customer/save")
    public ResponseEntity<?> save(@RequestBody Customer model) {
        try {
            if (model == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

            var result = service.save(model);
            if (result != null && result.resultType == ActionResultType.OK)
                return new ResponseEntity(HttpStatus.OK);
            else
                return ResponseEntity.badRequest().body(result.message);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body(e.getMessage());
        }


    }


    @PostMapping("customer/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") long id) {
        try {

            Customer cus = new Customer();
            cus.setId(id);

            service.delete(cus);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }


}
