package digitalvision.customer;

import digitalvision.EmailUtil;
import digitalvision.common.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CustomerService implements IGenaricService<Customer> {
    @Autowired
    private ICustomerRepository repo;

    @Override
    public List<Customer> getAll() {
        List<Customer> customers = new ArrayList<Customer>();
        repo.findAll(1).forEach(customers::add);
        return customers;
    }

    @Override
    public Customer getById(Customer t) {
        Customer customer = repo.findById(t.getId()).get();
        return customer;
    }

    @Override
    public ActionResult save(Customer t) {
        try {
            System.out.println(t.getEmail());
            var validateMessage = validateCustomer(t);
            if (validateMessage != null && validateMessage != "") {
                return new ActionResult(ActionResultType.Error, validateMessage);
            }

            if (t.getId() == 0) {
                SimpleDateFormat sdf = new SimpleDateFormat("mmHHssMMdd");
                var code = "D" + sdf.format(new Date());
                t.setActivationCode(code);
                t.setState(StateEnum.INACTIVE);
                new EmailUtil().sendActivationCode(t.getActivationCode(), t.getEmail(), t.getFirstName());
            }
            repo.save(t);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }

    }

    @Override
    public ActionResult delete(Customer t) {

        try {
            var customer = getById(t);
            customer.setState(StateEnum.DELETED);
            repo.save(customer);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }
    }

    public LoginResponse login(LoginRequest request) {

        var res = repo.login(request.getUserName());
        System.out.println("DB " + res.getPassword());
        System.out.println("REQ " + request.getPassword());
        if (res == null) {
            return new LoginResponse(null, null, UserTypeEnum.Customer, null, new ActionResult(ActionResultType.NotExists, "Email has not registered"));
        } else {
            System.out.println("res.getPassword() = " + res.getPassword());
            System.out.println("request.getPassword() = " + request.getPassword());

            if (res.getState() == StateEnum.DELETED) {
                return new LoginResponse(res, null, UserTypeEnum.Customer, null, new ActionResult(ActionResultType.Terminated, "Customer has been terminated"));
            } else if (res.getPassword().equals(request.getPassword())) {
                if (res.getState() == StateEnum.INACTIVE) {
                    return new LoginResponse(res, null, UserTypeEnum.Customer, null, new ActionResult(ActionResultType.InActive, "Customer is not Activated"));
                } else {
                    return new LoginResponse(res, null, UserTypeEnum.Customer, null, new ActionResult(ActionResultType.OK, ""));
                }
            } else {
                return new LoginResponse(res, null, UserTypeEnum.Customer, null, new ActionResult(ActionResultType.BADRequest, "Passwords are not match"));
            }
        }
    }

    private String validateCustomer(Customer customer) {
        if (customer.getFirstName() == null || customer.getFirstName().trim().isEmpty()) {
            return "Please specify First name";
        } else if (customer.getLastname() == null || customer.getLastname().trim().isEmpty()) {
            return "Please specify Last name";
        } else if (customer.getEmail() == null || customer.getEmail().trim().isEmpty()) {
            return "Please specify email";
        } else if (customer.getContactNo() == null || customer.getContactNo().trim().isEmpty()) {
            return "Please specify email";
        } else if (customer.getBillingAddress() == null || customer.getBillingAddress().trim().isEmpty()) {
            return "Please specify billing address";
        } else if (customer.getId() == 0 && (customer.getPassword() == null || customer.getBillingAddress().trim().isEmpty())) {
            return "Please specify password";
        }

        if (repo.alreadyExistEmailOnCustomer(customer.getEmail(), customer.getId()) != null) {
            return "Email address already registed with a customer";
        }
        else if (repo.alreadyExistEmailOnAdminUser(customer.getEmail()) != null) {
            return "Email address already registed with a customer";
        }

        return "";
    }


    public LoginResponse activateAccount(long customerId, String activationCode) {
        var customer = repo.findById(customerId).get();
        if (customer == null)
            return new LoginResponse(customer, null, UserTypeEnum.Customer, null, new ActionResult(ActionResultType.InActive, "Customer is not Activated"));
        if (customer.getActivationCode().trim().equals(activationCode.trim())) {
            customer.setActivationCode("AlreadyActivatedAAAAA");
            customer.setState(StateEnum.ACTIVE);
            repo.save(customer);
            return new LoginResponse(customer, null, UserTypeEnum.Customer, null, new ActionResult(ActionResultType.OK, ""));
        } else
            return new LoginResponse(customer, null, UserTypeEnum.Customer, null, new ActionResult(ActionResultType.InActive, "Customer is not Activated"));

    }

}
