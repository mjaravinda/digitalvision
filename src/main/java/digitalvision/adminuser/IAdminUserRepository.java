package digitalvision.adminuser;

import digitalvision.customer.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface IAdminUserRepository extends CrudRepository<AdminUser, Long> {

    @Query(
            value = "SELECT * FROM admin_users c WHERE c.state = ?0",
            nativeQuery = true)
    List<AdminUser> findAll(int state);

    @Query(
            value = "SELECT * FROM admin_users c WHERE c.email = ?1",
            nativeQuery = true)
    Customer login(String username);
}
