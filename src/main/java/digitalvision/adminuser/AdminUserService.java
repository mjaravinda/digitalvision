package digitalvision.adminuser;

import digitalvision.common.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdminUserService implements IGenaricService<AdminUser> {
    @Autowired
    private IAdminUserRepository repo;

    @Override
    public List<AdminUser> getAll() {
        List<AdminUser> adminusers = new ArrayList<AdminUser>();
        repo.findAll(1).forEach(adminusers::add);
        return adminusers;
    }

    @Override
    public AdminUser getById(AdminUser t) {
        AdminUser adminuser = repo.findById(t.getId()).get();
        return adminuser;
    }

    @Override
    public ActionResult save(AdminUser t) {
        try {
            t.setState(StateEnum.ACTIVE);
            repo.save(t);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }

    }

    @Override
    public ActionResult delete(AdminUser t) {

        try {
            var adminuser = getById(t);
            adminuser.setState(StateEnum.DELETED);
            repo.save(adminuser);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }
    }


    public LoginResponse login(LoginRequest request) {
        var res = repo.login(request.getUserName());
        if (res == null) {
            return new LoginResponse(null, null, UserTypeEnum.AdminUser, null, new ActionResult(ActionResultType.BADRequest, "Email has not registered"));
        } else {

            if (res.getState() == StateEnum.DELETED) {
                return new LoginResponse(res, null, UserTypeEnum.AdminUser, null, new ActionResult(ActionResultType.Terminated, "Customer has been terminated"));
            } else if (res.getPassword().equals(request.getPassword())) {
                if (res.getState() == StateEnum.INACTIVE) {
                    return new LoginResponse(res, null, UserTypeEnum.AdminUser, null, new ActionResult(ActionResultType.InActive, "Customer is not Activated"));
                } else {
                    return new LoginResponse(res, null, UserTypeEnum.AdminUser, null, new ActionResult(ActionResultType.OK, ""));
                }
            } else {
                return new LoginResponse(res, null, UserTypeEnum.AdminUser, null, new ActionResult(ActionResultType.BADRequest, "Passwords are not match"));
            }
        }
    }

    public LoginResponse activateAccount(long userId, String activationCode) {
        var user = repo.findById(userId).get();
        if (user == null)
            return new LoginResponse(null, user, UserTypeEnum.AdminUser, null, new ActionResult(ActionResultType.InActive, "User is not Activated"));
        if (user.getActivationCode().trim().equals(activationCode.trim())) {
            user.setActivationCode("AlreadyActivatedAAAAA");
            user.setState(StateEnum.ACTIVE);
            repo.save(user);
            return new LoginResponse(null, user, UserTypeEnum.Customer, null, new ActionResult(ActionResultType.OK, ""));
        } else
            return new LoginResponse(null, user, UserTypeEnum.Customer, null, new ActionResult(ActionResultType.InActive, "User is not Activated"));

    }

}

