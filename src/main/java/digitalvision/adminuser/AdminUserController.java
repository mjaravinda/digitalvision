package digitalvision.adminuser;

import digitalvision.EmailUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "adminuser", description = "used for view,add, edit and delete adminusers")
@RequestMapping("/api/")
@RestController
public class AdminUserController {
    @Autowired
    private AdminUserService service;

    @ApiOperation(value = "View a list of available adminusers", response = Iterable.class)
    @GetMapping("adminuser/all")
    public List<AdminUser> getAll() {

        return service.getAll();
    }

    @GetMapping("adminuser/getbyid/{id}")
    public AdminUser getById(@PathVariable("id") int id) {

        AdminUser brnd = new AdminUser();
        brnd.setId(id);
        return service.getById(brnd);
    }


    @PostMapping("adminuser/save")
    public ResponseEntity save(@RequestBody AdminUser model) {
        try {
            if (model == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

            service.save(model);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("adminuser/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int id) {
        try {

            AdminUser brnd = new AdminUser();
            brnd.setId(id);

            service.delete(brnd);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("testmail")
    public ResponseEntity TestMail() {
        try {
            new EmailUtil().sendActivationCode("G3233434", "mjaravinda@gmail.com", "Janaka");

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ResponseEntity(HttpStatus.OK);
    }
}
