package digitalvision.adminuser;

import com.fasterxml.jackson.annotation.JsonIgnore;
import digitalvision.common.StateEnum;
import digitalvision.order.Order;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "admin_users")
public class AdminUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private StateEnum State;
    private java.sql.Timestamp lastAttempedTime;
    private int attempedCount;


    private String firstName;
    private String lastname;
    private String email;
    private String password;
    private String contactNo;

    @JsonIgnore
    @OneToMany(mappedBy = "ApprovedBy")
    private List<Order> orders;
    private String activationCode;

    public AdminUser() {
        super();
        // TODO Auto-generated constructor stub
    }

    public AdminUser(long id, StateEnum state, Timestamp lastAttempedTime, int attempedCount, String firstName,
                     String lastname, String email, String password, String contactNo, String activationCode) {
        super();
        this.id = id;
        State = state;
        this.lastAttempedTime = lastAttempedTime;
        this.attempedCount = attempedCount;
        this.firstName = firstName;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.contactNo = contactNo;
        this.activationCode = activationCode;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public StateEnum getState() {
        return State;
    }

    public void setState(StateEnum state) {
        State = state;
    }

    public java.sql.Timestamp getLastAttempedTime() {
        return lastAttempedTime;
    }

    public void setLastAttempedTime(java.sql.Timestamp lastAttempedTime) {
        this.lastAttempedTime = lastAttempedTime;
    }

    public int getAttempedCount() {
        return attempedCount;
    }

    public void setAttempedCount(int attempedCount) {
        this.attempedCount = attempedCount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

}
