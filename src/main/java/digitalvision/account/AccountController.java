package digitalvision.account;

import digitalvision.adminuser.AdminUserService;
import digitalvision.common.ActionResultType;
import digitalvision.common.LoginRequest;
import digitalvision.common.LoginResponse;
import digitalvision.customer.CustomerService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "account", description = "used for Access User settings")
@RequestMapping("/api/")
@RestController
public class AccountController {

    @Autowired
    private CustomerService cusRepo;
    @Autowired
    private AdminUserService userRepo;

    @PostMapping("login")
    public LoginResponse login(@RequestBody LoginRequest model) {
        var res = cusRepo.login(model);

        if (res == null || res.getResult().resultType == ActionResultType.NotExists || res.getResult().resultType == ActionResultType.Terminated) {
            res = userRepo.login(model);
        }

        return res;
    }

    @PostMapping("activateUser/{id}/{activatecode}/{usertype}")
    public LoginResponse login(@PathVariable("id") long id, @PathVariable("activatecode") String code, @PathVariable("usertype") int usertype) {
        System.out.println(id + " " + code + " | " + usertype);
        LoginResponse res = null;
        if (usertype == 0)
            res = cusRepo.activateAccount(id, code);
        else
            res = userRepo.activateAccount(id, code);
        return res;
    }


}
