package digitalvision.producttype;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "productType", description = "used for view,add, edit and delete productTypes")
@RequestMapping("/api/")
@RestController
public class ProductTypeController {
    @Autowired
    private ProductTypeService service;

    @ApiOperation(value = "View a list of available productTypes", response = Iterable.class)
    @GetMapping("productType/all")
    public List<ProductType> getAll() {

        return service.getAll();
    }

    @GetMapping("productType/getbyid/{id}")
    public ProductType getById(@PathVariable("id") int id) {

        ProductType p = new ProductType();
        p.setId(id);
        return service.getById(p);
    }


    @PostMapping("productType/save")
    public ResponseEntity save(@RequestBody ProductType model) {
        try {
            if (model == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

            service.save(model);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }


    @PostMapping("productType/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int id) {
        try {

            ProductType p = new ProductType();
            p.setId(id);

            service.delete(p);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

}
