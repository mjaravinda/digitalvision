package digitalvision.producttype;

import com.fasterxml.jackson.annotation.JsonIgnore;
import digitalvision.Product.Product;
import digitalvision.common.StateEnum;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "productTypes")
public class ProductType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private StateEnum state;
    private String productTypeName;

    @JsonIgnore
    @OneToMany(mappedBy = "productType")
    private List<Product> products;


    public ProductType(int id, StateEnum state, String productTypeName, List<Product> products) {
        super();
        this.id = id;
        this.state = state;
        this.productTypeName = productTypeName;
        this.products = products;
    }

    public ProductType() {
        super();
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StateEnum getState() {
        return state;
    }

    public void setState(StateEnum state) {
        this.state = state;
    }

    public String getproductTypeName() {
        return productTypeName;
    }

    public void setproductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

}
