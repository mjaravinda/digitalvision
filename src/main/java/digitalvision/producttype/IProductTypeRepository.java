package digitalvision.producttype;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface IProductTypeRepository extends CrudRepository<ProductType, Integer> {

    @Query(
            value = "SELECT * FROM product_types c WHERE c.state =?1",
            nativeQuery = true)
    List<ProductType> findAll(int state);
}
