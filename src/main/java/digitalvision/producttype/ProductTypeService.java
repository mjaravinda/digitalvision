package digitalvision.producttype;

import digitalvision.common.ActionResult;
import digitalvision.common.ActionResultType;
import digitalvision.common.IGenaricService;
import digitalvision.common.StateEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductTypeService implements IGenaricService<ProductType> {
    @Autowired
    private IProductTypeRepository repo;

    @Override
    public List<ProductType> getAll() {
        List<ProductType> productTypes = new ArrayList<ProductType>();
        repo.findAll(1).forEach(productTypes::add);
        return productTypes;
    }

    @Override
    public ProductType getById(ProductType t) {
        ProductType productType = repo.findById(t.getId()).get();
        return productType;
    }

    @Override
    public ActionResult save(ProductType t) {
        try {
            t.setState(StateEnum.ACTIVE);
            repo.save(t);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }

    }

    @Override
    public ActionResult delete(ProductType t) {

        try {
            var productType = getById(t);
            productType.setState(StateEnum.DELETED);
            repo.save(productType);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }
    }

}
