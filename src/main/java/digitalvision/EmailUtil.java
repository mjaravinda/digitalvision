package digitalvision;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

public class EmailUtil {

    /**
     * Utility method to send simple HTML email
     *
     * @param session
     * @param toEmail
     * @param subject
     * @param body
     */
    public static void sendEmail(Session session, String toEmail, String subject, String body) {
        try {
            MimeMessage msg = new MimeMessage(session);
            //set message headers
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress("digitalvision.noreply@janaksystems.com", "NoReply-DigitalVision"));
            msg.setReplyTo(InternetAddress.parse("digitalvision.noreply@janaksystems.com", false));
            msg.setSubject(subject, "UTF-8");
            msg.setContent(body, "text/html; charset=utf-8");
            msg.setSentDate(new Date());

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
            System.out.println("Message is ready");
            Transport.send(msg);

            System.out.println("EMail Sent Successfully!!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendActivationCode(String activationCode, String toEmail, String toName) {
        final String fromEmail = "digitalvision.noreply@janaksystems.com";
//        final String password = "Janaka123";

        System.out.println("Email Start");
        Properties props = new Properties();
//        props.put("mail.smtp.host", "ultra33.lankahost.net"); //SMTP Host
//        props.put("mail.smtp.port", "465"); //TLS Port
//        props.put("mail.smtp.auth", "true"); //enable authentication
//        props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
//        props.put("mail.smtp.ssl.enable", "true"); //enable STARTTLS

        final String username = "b501d02ee7abf1";
        final String password = "93d28fb911d8ab";

        props.put("mail.smtp.host", "smtp.mailtrap.io"); //SMTP Host
        props.put("mail.smtp.port", "2525"); //TLS Port
        props.put("mail.smtp.auth", "true"); //enable authentication
        props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
        props.put("mail.smtp.ssl.enable", "false"); //enable STARTTLS

        //create Authenticator object to pass in Session.getInstance argument
        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        };

        Session session = Session.getInstance(props, auth);

        String bodyContent = "<div  style=\"width:100%; height:200px; background-color:#CFF0F5; font-family: Arial, Helvetica, sans-serif; border-radius:20px\">\r\n"
                + "<p>&nbsp;</p>\r\n"
                + "<h4 style=\"text-align:center; margin-top:20px\">Hello " + toName + "</h4>\r\n"
                + "<p style=\"text-align:center\">Thank you for registering with us. <br />Please use following Activation code for activate your account.</p>\r\n"
                + "	<p style=\"text-align:center;font-size:35px; font-weight:bold;\">" + activationCode + "</p>\r\n"
                + "</div>";

        EmailUtil.sendEmail(session, toEmail, "DigitalVision Activation", bodyContent);
    }
}
