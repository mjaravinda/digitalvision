package digitalvision.Product;

import digitalvision.brand.IBrandRepository;
import digitalvision.common.ActionResult;
import digitalvision.common.ActionResultType;
import digitalvision.common.IGenaricService;
import digitalvision.common.StateEnum;
import digitalvision.producttype.IProductTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService implements IGenaricService<Product> {
    @Autowired
    private IProductRepository repo;
    private IBrandRepository brandrepo;
    private IProductTypeRepository ptyperepo;

    @Override
    public List<Product> getAll() {
        List<Product> products = new ArrayList<Product>();
        var dbproducts = repo.findAll(1);
        if (dbproducts != null) {
            for (Product product : dbproducts) {
                product.setBrandId(product.getBrand().getId());
                product.setProductTypeId(product.getProductType().getId());
                products.add(product);
            }

        }
        return products;
    }

    public List<Product> getAll(ProductFilter filter) {
        List<Product> products = new ArrayList<Product>();
        var dbproducts = repo.findAll(1);
        var filteredBrands = filter.getBrandIds();
        var filteredcategories = filter.getProdcutTypes();

        for (Product product : dbproducts) {
            if (filteredBrands != null && filteredBrands.size() > 0 && !filteredBrands.contains(product.getBrandId())) {
                continue;
            }

            if (filteredcategories != null && filteredcategories.size() > 0 && !filteredcategories.contains(product.getProductTypeId())) {
                continue;
            }
            if (filter.getStartPrice() > 0 && product.getUnitPrice() < filter.getStartPrice()) {
                continue;
            }


            if (filter.getEndPrice() != 0 && product.getUnitPrice() > filter.getEndPrice()) {
                continue;
            }

            if (filter.getSearchCol() != null && filter.getSearchCol() != "" &&
                    !(
                            product.getProductName().contains(filter.getSearchCol()) ||
                                    product.getProductDescription().contains(filter.getSearchCol())
                    )
            ) {
                continue;
            }
            products.add(product);


        }


        return products;
    }

    @Override
    public Product getById(Product t) {
        Product p = repo.findById(t.getId()).get();
        return p;
    }

    @Override
    public ActionResult save(Product t) {
        try {
//			t.setBrand(brandrepo.findById(t.getBrandId()).get());
//			t.setProductType(ptyperepo.findById(t.getProductTypeId()).get());
//			repo.save(t);
            t.setState(StateEnum.ACTIVE);
            if (t.getId() == 0)
                repo.insertProduct(t.getProductCode(), t.getProductDescription(), t.getProductName(), 1, t.getUnitPrice(), t.getBrandId(), t.getProductTypeId(), t.getImage1(), t.getImage2(), t.getImage3(), t.getImage4());
            else
                repo.updateProduct(t.getProductCode(), t.getProductDescription(), t.getProductName(), 1, t.getUnitPrice(), t.getBrandId(), t.getProductTypeId(), t.getImage1(), t.getImage2(), t.getImage3(), t.getImage4(), t.getId());
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }

    }

    @Override
    public ActionResult delete(Product t) {

        try {
            var p = getById(t);
            p.setState(StateEnum.DELETED);
            repo.save(p);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }
    }

}

