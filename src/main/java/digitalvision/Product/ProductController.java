package digitalvision.Product;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "product", description = "used for view, search, add, edit and delete product")
@RequestMapping("/api/")
@RestController
public class ProductController {
    @Autowired
    private ProductService service;

    @ApiOperation(value = "View a list of available products", response = Iterable.class)
    @GetMapping("product/all")
    public List<Product> getAll() {

        return service.getAll();
    }

    @ApiOperation(value = "Product Filter By Options", response = Iterable.class)
    @PostMapping("product/filter")
    public List<Product> getFilter(@RequestBody ProductFilter model) {

        return service.getAll(model);
    }

    @GetMapping("product/getbyid/{id}")
    public Product getById(@PathVariable("id") int id) {

        Product p = new Product();
        p.setId(id);
        return service.getById(p);
    }


    @PostMapping("product/save")
    public ResponseEntity save(@RequestBody Product model) {
        try {
            if (model == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

            service.save(model);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("product/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int id) {
        try {

            Product p = new Product();
            p.setId(id);

            service.delete(p);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

}
