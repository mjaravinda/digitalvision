package digitalvision.Product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import digitalvision.brand.Brand;
import digitalvision.common.StateEnum;
import digitalvision.order.OrderItem;
import digitalvision.producttype.ProductType;
import digitalvision.wishlist.WishList;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String productName;
    private String productCode;
    private float unitPrice;
    private StateEnum state;
    private String image1;
    private String image2;
    private String image3;
    private String image4;
    private String productDescription;
    @JsonInclude()
    @Transient
    private int productTypeId;
    @JsonIgnore
    @OneToMany(mappedBy = "product")
    private List<WishList> wishLists;
    @JsonIgnore
    @OneToMany(mappedBy = "product")
    private List<OrderItem> orderItems;
    @JsonInclude()
    @Transient
    private int brandId;
    @ManyToOne
    @JoinColumn(name = "productTypeId")
    private ProductType productType;
    @ManyToOne
    @JoinColumn(name = "brandId")
    private Brand brand;

    public Product(long id, String productName, String productCode, float unitPrice, StateEnum state, String image1,
                   String image2, String image3, String image4, String productDescription, int productTypeId, int brandId,
                   ProductType productType, Brand brand) {
        super();
        this.id = id;
        this.productName = productName;
        this.productCode = productCode;
        this.unitPrice = unitPrice;
        this.state = state;
        this.image1 = image1;
        this.image2 = image2;
        this.image3 = image3;
        this.image4 = image4;
        this.productDescription = productDescription;
        this.productTypeId = productTypeId;
        this.brandId = brandId;
        this.productType = productType;
        this.brand = brand;
    }

    public Product() {
        super();
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    public List<WishList> getWishLists() {
        return wishLists;
    }

    public void setWishLists(List<WishList> wishLists) {
        this.wishLists = wishLists;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public StateEnum getState() {
        return state;
    }

    public void setState(StateEnum state) {
        this.state = state;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

}
