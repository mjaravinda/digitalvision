package digitalvision.Product;

import java.util.ArrayList;

public class ProductFilter {
    private ArrayList<Integer> brandIds;
    private ArrayList<Integer> prodcutTypes;
    private Float startPrice;
    private Float endPrice;
    private String searchCol;
    private String sortingCol;

    public ProductFilter(ArrayList<Integer> brandIds, ArrayList<Integer> prodcutTypes, Float startPrice, Float endPrice,
                         String searchCol, String sortingCol) {
        super();
        this.brandIds = brandIds;
        this.prodcutTypes = prodcutTypes;
        this.startPrice = startPrice;
        this.endPrice = endPrice;
        this.searchCol = searchCol;
        this.sortingCol = sortingCol;
    }

    public ArrayList<Integer> getBrandIds() {
        return brandIds;
    }

    public void setBrandIds(ArrayList<Integer> brandIds) {
        this.brandIds = brandIds;
    }

    public ArrayList<Integer> getProdcutTypes() {
        return prodcutTypes;
    }

    public void setProdcutTypes(ArrayList<Integer> prodcutTypes) {
        this.prodcutTypes = prodcutTypes;
    }

    public Float getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Float startPrice) {
        this.startPrice = startPrice;
    }

    public Float getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(Float endPrice) {
        this.endPrice = endPrice;
    }

    public String getSearchCol() {
        return searchCol;
    }

    public void setSearchCol(String searchCol) {
        this.searchCol = searchCol;
    }

    public String getSortingCol() {
        return sortingCol;
    }

    public void setSortingCol(String sortingCol) {
        this.sortingCol = sortingCol;
    }


}
