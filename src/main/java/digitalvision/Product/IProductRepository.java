package digitalvision.Product;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface IProductRepository extends CrudRepository<Product, Long> {

    @Query(
            value = "SELECT * FROM products c WHERE c.state =?1",
            nativeQuery = true)
    List<Product> findAll(int state);

    @Modifying
    @Query(value = "INSERT INTO `products`\r\n"
            + "(id,"
            + "`product_code`,"
            + "`product_description`,"
            + "`product_name`,"
            + "`state`,"
            + "`unit_price`,"
            + "`brand_id`,"
            + "`product_type_id`"
            + ",`image1`,"
            + "`image2`,"
            + "`image3"
            + "`,`image4`)  "
            + "SELECT   IFNULL(MAX(Id),0)+1,?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11 FROM products;"
            , nativeQuery = true)
    void insertProduct(
            String productCode,
            String productDescrption,
            String name,
            int state,
            float unitPrice,
            int bandId,
            int ptid,
            String image1,
            String image2,
            String image3,
            String image4

    );

    @Modifying
    @Query(value = "UPDATE `products` SET"
            + "`product_code` = ?1,"
            + "`product_description` =?2,"
            + "`product_name`=?3,"
            + "`state` =?4,"
            + "`unit_price` =?5,"
            + "`brand_id`=?6,"
            + "`product_type_id` =?7"
            + ",`image1`=?8,"
            + "`image2`=?9,"
            + "`image3` = ?10"
            + ",`image4` =?11)  "
            + "WHERE id = ?12"
            , nativeQuery = true)
    void updateProduct(
            String productCode,
            String productDescrption,
            String name,
            int state,
            float unitPrice,
            int bandId,
            int ptid,
            String image1,
            String image2,
            String image3,
            String image4,
            long id
    );
}

