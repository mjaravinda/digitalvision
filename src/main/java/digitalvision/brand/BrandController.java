package digitalvision.brand;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "brand", description = "used for view,add, edit and delete brands")
@RequestMapping("/api/")
@RestController
public class BrandController {
    @Autowired
    private BrandService service;

    @ApiOperation(value = "View a list of available brands", response = Iterable.class)
    @GetMapping("brand/all")
    public List<Brand> getAll() {
        return service.getAll();
    }

    @GetMapping("brand/getbyid/{id}")
    public Brand getById(@PathVariable("id") int id) {
        Brand brnd = new Brand();
        brnd.setId(id);
        return service.getById(brnd);
    }

    @PostMapping("brand/save")
    public ResponseEntity save(@RequestBody Brand model) {
        try {
            if (model == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

            service.save(model);
            return new ResponseEntity(model, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("brand/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int id) {
        try {
            Brand brnd = new Brand();
            brnd.setId(id);

            service.delete(brnd);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

}
