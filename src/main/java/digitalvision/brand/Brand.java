package digitalvision.brand;

import com.fasterxml.jackson.annotation.JsonIgnore;
import digitalvision.Product.Product;
import digitalvision.common.StateEnum;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "brands")
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private StateEnum state;
    private String brandName;
    private String brandLogo;
    @JsonIgnore
    @OneToMany(mappedBy = "brand")
    private List<Product> products;

    public Brand(int id, StateEnum state, String brandName, String brandLogo, List<Product> products) {
        super();
        this.id = id;
        this.state = state;
        this.brandName = brandName;
        this.brandLogo = brandLogo;
        this.products = products;
    }

    public Brand() {
        super();
    }

    public StateEnum getState() {
        return state;
    }

    public void setState(StateEnum state) {
        this.state = state;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
