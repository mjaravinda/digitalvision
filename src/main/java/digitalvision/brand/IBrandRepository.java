package digitalvision.brand;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface IBrandRepository extends CrudRepository<Brand, Integer> {

    @Query(
            value = "SELECT * FROM brands  WHERE state =?1",
            nativeQuery = true)
    List<Brand> findAll(int state);
}
