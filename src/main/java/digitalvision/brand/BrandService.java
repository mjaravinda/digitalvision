package digitalvision.brand;

import digitalvision.common.ActionResult;
import digitalvision.common.ActionResultType;
import digitalvision.common.IGenaricService;
import digitalvision.common.StateEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BrandService implements IGenaricService<Brand> {
    @Autowired
    private IBrandRepository repo;

    @Override
    public List<Brand> getAll() {
        List<Brand> brands = new ArrayList<Brand>();
        repo.findAll(1).forEach(brands::add);
        return brands;
    }

    @Override
    public Brand getById(Brand t) {
        Brand brand = repo.findById(t.getId()).get();
        return brand;
    }

    @Override
    public ActionResult save(Brand t) {
        try {
            t.setState(StateEnum.ACTIVE);
            repo.save(t);
            return new ActionResult(ActionResultType.OK, "Brand created successfully.");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }

    }

    @Override
    public ActionResult delete(Brand t) {

        try {
            var brand = getById(t);
            brand.setState(StateEnum.DELETED);
            repo.save(brand);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }
    }

}
