package digitalvision.wishlist;

import digitalvision.common.ActionResult;
import digitalvision.common.ActionResultType;
import digitalvision.common.IGenaricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WishListService implements IGenaricService<WishList> {
    @Autowired
    private IWishListRepository repo;

    @Override
    public List<WishList> getAll() {
        List<WishList> wishlists = new ArrayList<WishList>();
        repo.findAll().forEach(wishlists::add);
        return wishlists;
    }


    public List<WishList> getAll(long customerId) {
        List<WishList> wishlists = new ArrayList<WishList>();
        repo.findAll(customerId).forEach(wishlists::add);
        return wishlists;
    }

    @Override
    public WishList getById(WishList t) {
        WishList wishlist = repo.findById(t.getId()).get();
        return wishlist;
    }

    @Override
    public ActionResult save(WishList t) {
        try {
            repo.save(t);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }

    }

    @Override
    public ActionResult delete(WishList t) {

        try {
            var wishlist = getById(t);

            repo.delete(wishlist);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }
    }

    public ActionResult delete(long custoemrId) {

        try {
            repo.delete(custoemrId);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }
    }
}