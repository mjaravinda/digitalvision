package digitalvision.wishlist;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IWishListRepository extends CrudRepository<WishList, Long> {

    @Query(
            value = "SELECT * FROM wishlists c WHERE c.customer_id =?1",
            nativeQuery = true)
    List<WishList> findAll(long customerid);

    @Modifying
    @Query(value = "DELETE FROM wishlists where customer_id =?1"
            , nativeQuery = true)
    void delete(Long customerid);
}