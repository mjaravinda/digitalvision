package digitalvision.wishlist;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "wishlist", description = "used for view,add, edit and delete wishlists")
@RequestMapping("/api/")
@RestController
public class WishListController {
    @Autowired
    private WishListService service;

    @ApiOperation(value = "View a list of available wishlists", response = Iterable.class)
    @GetMapping("wishlist/all")
    public List<WishList> getAll() {

        return service.getAll();
    }

    @ApiOperation(value = "View a list of available wishlists of customer", response = Iterable.class)
    @GetMapping("wishlist/allbycustomer")
    public List<WishList> getAllByCustomer(@PathVariable("id") long id) {

        return service.getAll(id);
    }

    @GetMapping("wishlist/getbyid/{id}")
    public WishList getById(@PathVariable("id") long id) {

        WishList wlist = new WishList();
        wlist.setId(id);
        return service.getById(wlist);
    }


    @PostMapping("wishlist/save")
    public ResponseEntity save(@RequestBody WishList model) {
        try {
            if (model == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

            service.save(model);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }


    @PostMapping("wishlist/delete/{id}")
    public ResponseEntity deleteWishList(@PathVariable("id") long id) {
        try {

            WishList wlist = new WishList();
            wlist.setId(id);

            service.delete(wlist);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @PostMapping("wishlist/deleteAllByCustomer/{id}")
    public ResponseEntity delete(@PathVariable("id") long customerId) {
        try {


            service.delete(customerId);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

}
