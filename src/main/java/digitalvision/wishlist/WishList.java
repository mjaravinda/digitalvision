package digitalvision.wishlist;

import com.fasterxml.jackson.annotation.JsonInclude;
import digitalvision.Product.Product;
import digitalvision.customer.Customer;

import javax.persistence.*;

@Entity
@Table(name = "wishlists")
public class WishList {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "customerId")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "productId")
    private Product product;


    @JsonInclude()
    @Transient
    private long customerId;


    @JsonInclude()
    @Transient
    private long productId;


    public WishList(long id, Customer customer, Product product, long customerId, long productId) {
        super();
        this.id = id;
        this.customer = customer;
        this.product = product;
        this.customerId = customerId;
        this.productId = productId;
    }


    public WishList() {
        super();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

}
