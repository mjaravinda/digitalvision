package digitalvision.order;

public enum PaymentMethodEnum {
    CreditCard,
    CashOnDelivery
}
