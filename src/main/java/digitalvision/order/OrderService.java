package digitalvision.order;

import digitalvision.common.ActionResult;
import digitalvision.common.ActionResultType;
import digitalvision.common.IGenaricService;
import digitalvision.common.StateEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService implements IGenaricService<Order> {
    @Autowired
    private IOrderRepository repo;

    @Override
    public List<Order> getAll() {
        List<Order> orders = new ArrayList<Order>();
        repo.findAll(1).forEach(orders::add);
        return orders;
    }

    public List<Order> getAll(int status) {
        List<Order> orders = new ArrayList<Order>();
        repo.findAll(1).forEach(orders::add);
        return orders;
    }

    public List<Order> getAll(long customerId) {
        List<Order> orders = new ArrayList<Order>();
        repo.findAll(1).forEach(orders::add);
        return orders;
    }

    @Override
    public Order getById(Order t) {
        Order order = repo.findById(t.getId()).get();
        return order;
    }

    @Override
    public ActionResult save(Order t) {
        try {
            repo.save(t);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }

    }

    public void setOrderStatus(long orderid, long statusId) {


    }

    @Override
    public ActionResult delete(Order t) {

        try {
            var order = getById(t);
            order.setState(StateEnum.DELETED);
            repo.save(order);
            return new ActionResult(ActionResultType.OK, "");
        } catch (Exception e) {
            return new ActionResult(ActionResultType.Error, e.getMessage());
        }
    }

}
