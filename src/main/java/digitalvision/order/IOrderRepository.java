package digitalvision.order;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IOrderRepository extends CrudRepository<Order, Long> {

    @Query(
            value = "select * from orders where orderstatus=?1",
            nativeQuery = true)
    List<Order> findAll(int status);

    @Query(
            value = "select * from orders where customer_id=?1",
            nativeQuery = true)
    List<Order> findAll(long customerId);

    @Modifying
    @Query(value = "UPDATE orders SET orderstatus= ?2 WHERE id=?1", nativeQuery = true)
    void setStatus(long orderId, int status);
}