package digitalvision.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import digitalvision.Product.Product;

import javax.persistence.*;

@Entity
@Table(name = "orderitems")
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    @JoinColumn(name = "orderid")
    private Order order;
    @JsonInclude()
    @Transient
    private long orderId;

    @ManyToOne
    @JoinColumn(name = "productId")
    private Product product;
    private int qty;
    private float unitPrice;
    private float totalPrice;

    public OrderItem(long id, Order order, long orderId, Product product, int qty, float unitPrice,
                     float totalPrice) {
        super();
        this.id = id;
        this.order = order;
        this.orderId = orderId;
        this.product = product;
        this.qty = qty;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
    }

    public OrderItem() {
        super();
        // TODO Auto-generated constructor stub
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

}
