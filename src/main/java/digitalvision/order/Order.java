package digitalvision.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import digitalvision.adminuser.AdminUser;
import digitalvision.common.StateEnum;
import digitalvision.customer.Customer;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private StateEnum state;
    private String orderNo;
    private float totalAmount;

    @ManyToOne
    @JoinColumn(name = "customer")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "adminUser")
    private AdminUser ApprovedBy;
    @JsonInclude()
    @Transient
    private long customerid;
    private java.sql.Timestamp lastAttempedTime;
    private int shipperId;
    private PaymentMethodEnum paymentmethod;
    private String paymentrefNo;
    private OrderStatusEnum orderStatus;
    @OneToMany(mappedBy = "order")
    private List<OrderItem> orderItems;

    public Order(long id, StateEnum state, String orderNo, float totalAmount, Customer customer, long customerid,
                 Timestamp lastAttempedTime, int shipperId, PaymentMethodEnum paymentmethod, String paymentrefNo,
                 OrderStatusEnum orderStatus, List<OrderItem> orderItems) {
        super();
        this.id = id;
        this.state = state;
        this.orderNo = orderNo;
        this.totalAmount = totalAmount;
        this.customer = customer;
        this.customerid = customerid;
        this.lastAttempedTime = lastAttempedTime;
        this.shipperId = shipperId;
        this.paymentmethod = paymentmethod;
        this.paymentrefNo = paymentrefNo;
        this.orderStatus = orderStatus;
        this.orderItems = orderItems;
    }

    public Order() {
        super();
        // TODO Auto-generated constructor stub
    }

    public AdminUser getApprovedBy() {
        return ApprovedBy;
    }

    public void setApprovedBy(AdminUser approvedBy) {
        ApprovedBy = approvedBy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public StateEnum getState() {
        return state;
    }

    public void setState(StateEnum state) {
        this.state = state;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public long getCustomerid() {
        return customerid;
    }

    public void setCustomerid(long customerid) {
        this.customerid = customerid;
    }

    public java.sql.Timestamp getLastAttempedTime() {
        return lastAttempedTime;
    }

    public void setLastAttempedTime(java.sql.Timestamp lastAttempedTime) {
        this.lastAttempedTime = lastAttempedTime;
    }

    public int getShipperId() {
        return shipperId;
    }

    public void setShipperId(int shipperId) {
        this.shipperId = shipperId;
    }

    public PaymentMethodEnum getPaymentmethod() {
        return paymentmethod;
    }

    public void setPaymentmethod(PaymentMethodEnum paymentmethod) {
        this.paymentmethod = paymentmethod;
    }

    public String getPaymentrefNo() {
        return paymentrefNo;
    }

    public void setPaymentrefNo(String paymentrefNo) {
        this.paymentrefNo = paymentrefNo;
    }

    public OrderStatusEnum getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatusEnum orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }


}
