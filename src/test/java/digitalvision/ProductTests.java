package digitalvision;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import digitalvision.Product.Product;
import digitalvision.Product.ProductFilter;
import digitalvision.Product.ProductService;
import digitalvision.common.ActionResultType;

@SpringBootTest
public class ProductTests {
	@Autowired
	ProductService service;
	@Test
	public void testViewAllProducts() {
		
		var data = service.getAll();
		assertTrue(data.size() > 0);
		
	}
	@Test
	public void testSaveProduct() {
		var p = new Product();
		p.setBrandId(1);
		p.setProductTypeId(2);
		p.setProductName("Test Product");
		p.setUnitPrice(400);
		p.setProductCode("P0023");
		var r= service.save(p);
		assertEquals(r.resultType, ActionResultType.OK);
		
	}
	
	@Test
	public void testFilterProducts() {
		var filter= new ProductFilter(null, null,0f, 0f, "Test", null);
		var data = service.getAll(filter);
		assertTrue(data.size() > 0);
		
	}
}
