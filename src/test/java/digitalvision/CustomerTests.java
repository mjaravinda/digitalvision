package digitalvision;



import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import digitalvision.common.ActionResultType;
import digitalvision.common.LoginRequest;
import digitalvision.customer.Customer;
import digitalvision.customer.CustomerService;


@SpringBootTest
public class CustomerTests {
	@Autowired
	CustomerService service;
	double x = Math.random();
	
	@Test
	public void testRegister() {
		
		Customer c = new Customer();
		c.setFirstName("Test First Name");
		c.setLastname("Test Last Name");
		c.setEmail("test"+ x +"@gmail.com");
		c.setContactNo("22222222");
		c.setBillingAddress("123/45 Kandy Road yakkala");
		c.setPassword("A@43433");
		
		var r = service.save(c);
		assertEquals(r.resultType, ActionResultType.OK);
	}
	
	@Test
	public void testLoginNotActivated() {
		
		LoginRequest r = new LoginRequest();
		r.setPassword("A@43433");
		r.setUserName("test0.8990747805358211@gmail.com");
		var res = service.login(r);
		assertEquals(res.getResult().resultType,ActionResultType.InActive);
	
		
		
	}
}
