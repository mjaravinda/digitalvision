package digitalvision;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import digitalvision.producttype.*;


@SpringBootTest
public class ProductTypeTest  {
	@Autowired
	ProductTypeService service;
	
	@Test
	public void testView() {
		
		var data =service.getAll();
		assertNotNull(data);
	}
	
	
	@Test
	public void testCreate() {

		int oldsize =service.getAll().size();
		System.out.println("Old Size "+ oldsize);
		ProductType model = new ProductType();
		model.setId(0);
		model.setproductTypeName("Test ProductType");
	
		service.save(model);
		assertTrue(service.getAll().size() > oldsize);
		
	}
	@Test
	public void testUpdate() {
		var input = new ProductType();
		input.setId(10);
		var item = service.getById(input);
		item.setproductTypeName("New Test ProductType");
		
		service.save(item);
		
		assertEquals("New Test ProductType", service.getById(input).getproductTypeName());
	}
}
