package digitalvision;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import digitalvision.brand.Brand;
import digitalvision.brand.BrandService;


@SpringBootTest
public class BrandTests {
	@Autowired
	BrandService service;
	
	@Test
	public void testView() {		
		var data =service.getAll();
		assertNotNull(data);
	}	
	
	@Test
	public void testCreate() {

		int oldsize =service.getAll().size();
		System.out.println("Old Size "+ oldsize);
		Brand model = new Brand();
		model.setId(0);
		model.setBrandName("Test Brand");	
		service.save(model);
		assertTrue(service.getAll().size() > oldsize);
		
	}
	@Test
	public void testUpdate() {
		var input = new Brand();
		input.setId(3);
		var item = service.getById(input);
		item.setBrandName("New Test Brand");		
		service.save(item);		
		assertEquals("New Test Brand", service.getById(input).getBrandName());
		
		
	}
	
}
